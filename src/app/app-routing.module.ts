import { NgModule } from '@angular/core';
import { Routes, RouterModule  } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LancamentoDespesaComponent } from './tipo-lancamento/lancamento-despesa/lancamento-despesa.component';
import { LancamentoListComponent } from './tipo-lancamento/lancamento-list/lancamento-list.component';
import { LancamentoReceitaComponent } from './tipo-lancamento/lancamento-receita/lancamento-receita.component';

const routes: Routes = [
  {path: "", pathMatch: "full",redirectTo: "home"},
  {path: "home", component: HomeComponent},
  {path: "lancamento-despesa", component: LancamentoDespesaComponent},
  {path: "lancamento-list", component: LancamentoListComponent},
  {path: "lancamento-receita", component: LancamentoReceitaComponent}
  ];
  @NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }
  