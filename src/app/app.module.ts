import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LancamentoListComponent } from './tipo-lancamento/lancamento-list/lancamento-list.component';
import { LancamentoReceitaComponent } from './tipo-lancamento/lancamento-receita/lancamento-receita.component';
import { LancamentoDespesaComponent } from './tipo-lancamento/lancamento-despesa/lancamento-despesa.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LancamentoListComponent,
    HeaderComponent,
    FooterComponent,
    LancamentoDespesaComponent,
    LancamentoReceitaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
