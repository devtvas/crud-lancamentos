import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  lancamentos = [
    { id: 1, name: "João da Silva", description: "DESPESAS", email: "c001@email.com" },
    { id: 2, name: "Maria das Dores", description: "RECEITAS", email: "c002@email.com" },
    { id: 3, name: "Pedro Pedreira", description: "DESPESAS", email: "c003@email.com" },
    { id: 4, name: "Morgana Adams", description: "RECEITAS", email: "c004@email.com" }
  ];

  public getLancamentos(): Array<{ id, name, description, email }> {
    return this.lancamentos;
  }
  public createLancamento(lancamento: { id, name, description, email }) {
    this.lancamentos.push(lancamento);
  }
  constructor() {

  }
}