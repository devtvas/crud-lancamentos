import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lancamento-despesa',
  templateUrl: './lancamento-despesa.component.html',
  styleUrls: ['./lancamento-despesa.component.css']
})
export class LancamentoDespesaComponent implements OnInit {
  @Input() title:string = 'Despesas';
  constructor() { }

  ngOnInit(): void {
  }

}
