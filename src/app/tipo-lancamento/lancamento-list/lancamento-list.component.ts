import { Component, Input, OnInit } from '@angular/core';
import { DataService } from '../../data.service';

@Component({
  selector: 'lancamento-list',
  templateUrl: './lancamento-list.component.html',
  styleUrls: ['./lancamento-list.component.css'],
  // template: `<h3> {{ title }}</h3>`
})
export class LancamentoListComponent implements OnInit {
 @Input() title:string = 'Meus Lancamentos';
  lancamentos;
  selectedLancamento;
  constructor(public dataService: DataService) { }
  ngOnInit() {
    this.lancamentos = this.dataService.getLancamentos();
  }
  public selectLancamento(lancamento) {
    this.selectedLancamento = lancamento;

  }

}
