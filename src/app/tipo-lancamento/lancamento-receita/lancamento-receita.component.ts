import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lancamento-receita',
  templateUrl: './lancamento-receita.component.html',
  styleUrls: ['./lancamento-receita.component.css']
})
export class LancamentoReceitaComponent implements OnInit {
  @Input() title:string = 'Receitas';
  constructor() { }

  ngOnInit(): void {
  }

}
